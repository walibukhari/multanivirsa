<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CompleteOrder;
use App\Models\Order;
use App\Models\PlaceOrder;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(){
        $ordersNo = Product::select('id','name','product_id')->get();
        return view('admin.order.index',[
            'ordersNo' => $ordersNo
        ]);
    }

    public function getOrderNumbers(Request  $request){
        $ordersNo = Product::where('product_id', 'like', '%' . $request->term['term'] . '%')->select('id','name','product_id')->get();
        return response()->json($ordersNo);
    }

    public function getOrderData($data){
       $array = explode(',',$data);
       $product = Product::whereIn('id',$array)->get();
       return collect([
          'status' => true,
          'data' => $product
       ]);
    }

    public function createOrders(Request  $request){
        $data = Order::createOrder($request);
        return collect([
           'status' => true,
           'data' => $data
        ]);
    }

    public function orders(){
        $orders = Order::where('status','=',Order::STATUS_ADD_ORDER)->get();
        $placed_orders = Order::where('status','=',Order::STATUS_ORDER_PLACED)->get();
        $shipped_orders = Order::where('status','=',Order::STATUS_ORDER_SHIPPED)->get();
        $orders_failed = Order::where('status','=',Order::STATUS_ORDER_DELIVER_FAILED)->get();
        $orders_complete = CompleteOrder::with('product','order')->get();
        $orders = collect($orders)->unique('product_id');
        $placed_orders = collect($placed_orders)->unique('product_id');
        $shipped_orders = collect($shipped_orders)->unique('product_id');
        $orders_failed = collect($orders_failed)->unique('product_id');
        return view('admin.order.show',[
            'orders' => $orders,
            'orders_complete' => $orders_complete,
            'placed_orders' => $placed_orders,
            'shipped_orders' => $shipped_orders,
            'orders_failed' => $orders_failed
        ]);
    }

    public function placeOrder($order_id,$product_id,$status){
        $order = Order::where('product_id','=',$product_id)->where('status','=',$status)->get();
        PlaceOrder::orderPlaced($order);
        return collect([
           'status' => true,
           'message' => 'order placed successfully'
        ]);
    }

    public function deleteOrder($id,$product_id,$status){
        Order::where('product_id','=',$product_id)->where('status','=',$status)->delete();
        return collect([
           'status' => true,
            'message' => 'order deleted successfully'
        ]);
    }

    public function checkDetail($id,$status){
        $orders = Order::where('product_id','=',$id)->where('status','=',$status)->with('product.product_images')->get();
        $orders = collect($orders)->unique('product_id');
        return view('admin.order.detail',[
            'orders' => $orders
        ]);
    }

    public function shippedOrders($order_id,$product_id,$status){
        Order::where('product_id','=',$product_id)->where('status','=',$status)->update([
           'status' => Order::STATUS_ORDER_SHIPPED
        ]);
        return collect([
           'status' => true,
           'message' => 'order shipped successfully'
        ]);
    }

    public function paymentReceived($order_id,$product_id,$status){
        CompleteOrder::orderComplete($order_id,$product_id,$status);
        Order::where('product_id','=',$product_id)->where('status','=',$status)->update([
            'status' => Order::STATUS_ORDER_DELIVER_PAYMENT_RECEIVED_SUCCESS
        ]);
        return collect([
            'status' => true,
            'message' => 'order payment received successfully'
        ]);
    }

}
