<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\CompleteOrder;
use Illuminate\Http\Request;

class AccountsController extends Controller
{
    public function manageAccounts(){
        $orders_complete = CompleteOrder::with('product','order')->paginate(4);
        $total_orders = $this->getTotalOrders();
        $total_data = $this->getTotalData();
        $total_amount = $this->getTotalAmounts();
        $amounts = Account::all();
        return view('admin.account.index',[
            'orders_complete' => $orders_complete,
            'amounts' => $amounts,
            'total_profit' => $total_data['profit'],
            'total_sale_price' => $total_data['sale_price'],
            'total_original_price' => $total_data['original_price'],
            'total_orders' => $total_orders,
            'total_amount' => $total_amount
        ]);
    }

    public function getTotalOrders(){
        $total_order = CompleteOrder::count();
        return $total_order;
    }

    public function getTotalData(){
        $total_sale_price = CompleteOrder::with('product','order')->get();
        $profit = [];
        $sale_price = [];
        $original_price = [];
        foreach($total_sale_price as $total) {
            $total_profit = $total->order->sale_price - $total->order->original_price;
            $total_original_price = $total->order->original_price;
            $total_sale_price = $total->order->sale_price;
            array_push($profit,$total_profit);
            array_push($sale_price,$total_sale_price);
            array_push($original_price,$total_original_price);
        }
        $amount = array_sum($profit);
        $sale_sum = array_sum($sale_price);
        $original_sum = array_sum($original_price);
        return collect([
            'profit' => $amount,
            'sale_price' => $sale_sum,
            'original_price' => $original_sum
        ]);
    }

    public function getTotalAmounts(){
        $amounts = Account::all();
        $total = [];
        foreach ($amounts as $amount) {
            array_push($total,$amount->amount);
        }
        $total_amount = array_sum($total);
        return $total_amount;
    }

    public function addAccounts(Request  $request){
        Account::createAmount($request->all());
        return collect([
           'status' => true,
           'message' => 'amount added successfully'
        ]);
    }
}
