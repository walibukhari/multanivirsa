<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function addProduct(){
        return view('admin.product.add');
    }

    public function create(Request $request){
        Product::createProducts($request);
        return back()->with('success','Product Created Successfully');
    }

    public function list(){
        $products = Product::all();
        $trashedData = Product::onlyTrashed()->get();
        return view('admin.product.list',['products' => $products , 'trashedData' => $trashedData]);
    }

    public function delete($id) {
        Product::where('id','=',$id)->delete();
    }

    public function edit($id) {
        $product = Product::where('id','=',$id)->first();
        return view('admin.product.edit',[
           'product' => $product
        ]);
    }

    public function update(Request  $request , $id){
        Product::updateProduct($request ,$id);
        return redirect()->route('admin.list.product')->with('success','Product Update Successfully');
    }

    public function restore($id){
        Product::where('id','=',$id)->restore();
        return back()->with('success','Product Remove From Trashed Successfully');
    }
}

