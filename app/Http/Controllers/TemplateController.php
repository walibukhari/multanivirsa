<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TemplateController extends Controller
{
    //
     public function index()
    {
    	// dd(123);
    	return view('frontend.index');
    }

    public function shop()
    {
    	// dd(123);
    	return view('frontend.shop');
    }
    public function blog()
    {
    	// dd(123);
    	return view('frontend.blog');
    }
    public function contact()
    {
    	// dd(123);
    	return view('frontend.contact');
    }
    public function details()
    {
    	// dd(123);
    	return view('frontend.productDetail');
    }
    public function blogDetails()
    {
    	return view('frontend.blogDetails');
    }
    public function cart()
    {
    	return view('frontend.shopCart');
    }
    public function checkout()
    {
    	return view('frontend.checkOut');
    }
}
