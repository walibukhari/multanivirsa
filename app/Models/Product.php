<?php

namespace App\Models;

use App\Models\ProductImage;
use Illuminate\Database\Eloquent\Model;
use App\Traits\General;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use General , SoftDeletes;

    const STATUS_NEW = '';
    const STATUS_ADD_ORDER = 1;
    const STATUS_ORDER_PLACED = 2;
    const STATUS_ORDER_PLACED_BUT_NOT_DELIVER = 3;
    const STATUS_ORDER_DELIVER_SUCCESS = 4;
    const STATUS_ORDER_DELIVER_BUT_PAYMENT_NOT_RECEIVED = 5;
    const STATUS_ORDER_DELIVER_PAYMENT_RECEIVED_SUCCESS = 6;

    protected $fillable = [
        'name',
        'description',
        'image',
        'quantity',
        'original_price',
        'sale_price',
        'product_id'
    ];

    public static function createProducts($request){
        if($request->hasFile('files')) {
            $path2 = 'uploads/products';
            $filesName = self::uploadFiles($request->file('files'),$path2);
        }
        $getOrderId = self::orderId(self::checkTotalOrderNumbers());
        $product = self::create([
            'name' => $request->name,
            'description' => $request->description,
            'image' => $filesName[0],
            'quantity' => $request->qty,
            'original_price' => $request->original_price,
            'sale_price' => $request->sale_price,
            'product_id' => $getOrderId
        ]);
        return ProductImage::createProductImages($product->id , $filesName);
    }

    public static function checkTotalOrderNumbers(){
        return OrderNo::count();
    }

    public static function updateProduct($request , $id){
        if($request->hasFile('files')) {
            $path2 = 'uploads/products';
            $filesName = self::uploadFiles($request->file('files'),$path2);
        }
        self::where('id','=',$id)->update([
            'name' => $request->name,
            'description' => $request->description,
            'image' => $filesName[0],
            'quantity' => $request->qty,
            'original_price' => $request->original_price,
            'sale_price' => $request->sale_price
        ]);
        return ProductImage::createProductImages($id , $filesName);
    }

    public function product_images(){
        return $this->hasMany(ProductImage::class,'product_id','id');
    }

}
