<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FailOrder extends Model
{
    protected $fillable = [
        'order_id','product_id','status'
    ];
}
