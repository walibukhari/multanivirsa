<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'name' ,'amount' , 'remain_amount', 'total_amount', 'total_sale', 'total_sale', 'total_profit'
    ];

    public static function createAmount($request){
        return self::create([
            'name' => $request['name'],
            'amount' => $request['amount']
        ]);
    }
}
