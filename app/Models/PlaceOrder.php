<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceOrder extends Model
{
    protected $fillable = [
        'order_id','product_id','status'
    ];

    public static function orderPlaced($data){
        foreach ($data as $order) {
            self::create([
                'order_id' => $order->id,
                'product_id' => $order->product_id,
                'status' => Order::STATUS_ORDER_PLACED,
            ]);
        }
        self::changeOrdersTableStatus($data);
    }

    public static function changeOrdersTableStatus($order){
        foreach ($order as $orders) {
            Order::where('id','=',$orders->id)->update([
                'status' => Order::STATUS_ORDER_PLACED
            ]);
        }
    }

    public function order(){
        return $this->hasOne(Order::class,'id','order_id');
    }
}
