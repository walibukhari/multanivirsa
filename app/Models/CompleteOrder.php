<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompleteOrder extends Model
{
    protected $fillable = [
        'order_id','product_id','total_item','status'
    ];

    public static function orderComplete($id,$product_id,$status) {
        $data = Order::where('product_id','=',$product_id)->where('status','=',$status)->get();
        foreach($data as $order) {
            self::create([
                'order_id' => $order->id,
                'product_id' => $order->product_id,
                'status' => Order::STATUS_ORDER_DELIVER_PAYMENT_RECEIVED_SUCCESS,
            ]);
        }
    }

    public function product(){
        return $this->hasOne(Product::class,'id','product_id');
    }

    public function order(){
        return $this->hasOne(Order::class,'id','order_id');
    }

    public function getQuantityOrderComplete($id){
        return self::where('product_id','=',$id)->count();
    }

    public function getStatus($status){
        if($status == Order::STATUS_ORDER_DELIVER_PAYMENT_RECEIVED_SUCCESS){
            return 'PAID';
        }
    }

}
