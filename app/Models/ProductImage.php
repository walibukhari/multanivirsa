<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = [
        'product_id' , 'image'
    ];

    public static function createProductImages($product_id,$fileName){
        foreach ($fileName as $files) {
            self::create([
                'product_id' => $product_id,
                'image' => $files
            ]);
        }
    }
}
