<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderNo extends Model
{
    protected $fillable = [
        'order_no',
    ];

    public static function createOrderNumber($orderNo){
        return self::create([
            'order_no' => $orderNo
        ]);
    }
}
