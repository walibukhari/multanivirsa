<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const STATUS_NEW = '';
    const STATUS_ADD_ORDER = 1;
    const STATUS_ORDER_PLACED = 2;
    const STATUS_ORDER_PLACED_BUT_NOT_DELIVER = 3;
    const STATUS_ORDER_DELIVER_SUCCESS = 4;
    const STATUS_ORDER_SHIPPED = 5;
    const STATUS_ORDER_DELIVER_PAYMENT_RECEIVED_SUCCESS = 6;
    const STATUS_ORDER_DELIVER_FAILED = 7;

    protected $fillable = [
        'product_id',
        'order_no',
        'name',
        'image',
        'original_price',
        'sale_price',
        'status'
    ];

    public static function createOrder($request) {
        $data = Product::where('id','=',$request->id)->first();
        self::create([
            'product_id' => $data->id,
            'order_no' => $data->product_id,
            'name' => $data->name,
            'image' => $data->image,
            'original_price' => $data->original_price,
            'sale_price' => $data->sale_price,
            'status' => Product::STATUS_ADD_ORDER
        ]);
        return self::where('product_id','=',$request->id)->count();
    }

    public function getQuantity($id){
       return self::where('product_id','=',$id)->where('status','=',self::STATUS_ADD_ORDER)->count();
    }

    public function getQuantityPlaceOrder($id){
       return self::where('product_id','=',$id)->where('status','=',self::STATUS_ORDER_PLACED)->count();
    }

    public function getQuantityShippedOrder($id){
       return self::where('product_id','=',$id)->where('status','=',self::STATUS_ORDER_SHIPPED)->count();
    }

    public function product(){
        return $this->hasOne(Product::class,'id','product_id');
    }
}
