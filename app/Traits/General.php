<?php

namespace App\Traits;



use App\Models\OrderNo;

trait General
{
    public static function uploadFiles($files,$path){
        $arr = [];
        foreach($files as $file) {
            if ($file) {
                $original_name = strtolower(trim($file->getClientOriginalName()));
                $fileName = time().rand(100,999).$original_name;
                $fileName = $path.'/'.$fileName;
                $file->move($path, $fileName);
            }
            array_push($arr,$fileName);
        }
        return $arr;
    }

    public static function orderId($value){
        $orderId = [];
        $start_value = $value+1;
        array_push($orderId,sprintf("%03s",$start_value));
        $orderNo = 'MV-'.$orderId[0];
        OrderNo::createOrderNumber($orderNo);
        return $orderNo;
    }
}
