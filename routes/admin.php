<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::namespace('Auth\Admin')->group(function () {
    Route::get('login','LoginController@showLoginForm')->name('admin.login');
    Route::post('login','LoginController@login')->name('admin.login.post');
    Route::get('logout','LoginController@logout')->name('admin.logout');
});

Route::middleware(['adminAuth'])->namespace('Admin')->group(function () {
    Route::get('/home', 'AdminController@index')->name('admin.home');
    Route::get('/add-product', 'ProductController@addProduct')->name('admin.add.product');
    Route::post('/create-product', 'ProductController@create')->name('admin.create.product');
    Route::get('/edit-product/{id}', 'ProductController@edit')->name('admin.edit.product');
    Route::post('/update-product/{id}', 'ProductController@update')->name('admin.update.product');
    Route::get('/delete-product/{id}', 'ProductController@delete')->name('admin.delete.product');
    Route::get('/list-products', 'ProductController@list')->name('admin.list.product');
    Route::get('/restore-products/{id}', 'ProductController@restore')->name('admin.restore.product');

    Route::get('/add-order', 'OrderController@index')->name('admin.add.order');
    Route::get('/order-numbers', 'OrderController@getOrderNumbers')->name('admin.order.numbers');
    Route::get('/get/orders/data/{id}', 'OrderController@getOrderData')->name('admin.orders.get');
    Route::post('/create-orders', 'OrderController@createOrders')->name('admin.create.orders');
    Route::get('/orders', 'OrderController@orders')->name('admin.orders');
    Route::get('/place-order/{id}/{product_id}/{status}', 'OrderController@placeOrder')->name('admin.place.order');
    Route::get('/delete-order/{id}/{product_id}/{status}', 'OrderController@deleteOrder')->name('admin.delete.order');
    Route::get('/check-detail/{id}/{status}', 'OrderController@checkDetail')->name('admin.check.order.detail');
    Route::get('/shipped-orders/{id}/{product_id}/{status}', 'OrderController@shippedOrders')->name('admin.shipped.order');
    Route::get('/payment-received/{id}/{product_id}/{status}', 'OrderController@paymentReceived')->name('admin.payment-received.order');
    Route::get('/manage-accounts/', 'AccountsController@manageAccounts')->name('admin.manage.accounts');
    Route::post('/add-accounts/', 'AccountsController@addAccounts')->name('admin.add.accounts');
});
