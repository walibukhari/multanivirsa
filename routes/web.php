<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/template','TemplateController@index')->name('template.index');
Route::get('/shop','TemplateController@shop')->name('shop.index');
Route::get('/blog','TemplateController@blog')->name('blog.index');
Route::get('/contact','TemplateController@contact')->name('contact.index');
Route::get('/product-details','TemplateController@details')->name('details.index');
Route::get('/blog-details','TemplateController@blogDetails')->name('blogs.details');
Route::get('/shop-cart','TemplateController@cart')->name('shop.cart');
Route::get('/check-out','TemplateController@checkout')->name('checkout');