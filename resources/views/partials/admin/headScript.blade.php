<link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon"/>

<title>:: Soccer :: Project Dashboard</title>

<!-- Bootstrap Core and vandor -->
<link rel="stylesheet" href="{{asset('admin/assets/plugins/bootstrap/css/bootstrap.min.css')}}"/>

<!-- Plugins css -->
<link rel="stylesheet" href="{{asset('admin/assets/plugins/charts-c3/c3.min.css')}}"/>

<!-- Core css -->
<link rel="stylesheet" href="{{asset('admin/assets/css/main.css')}}"/>
<link rel="stylesheet" href="{{asset('admin/assets/css/theme1.css')}}"/>
