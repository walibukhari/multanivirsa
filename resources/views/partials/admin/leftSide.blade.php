<div id="left-sidebar" class="sidebar ">
    <h5 class="brand-name">Soccer <a href="javascript:void(0)" class="menu_option float-right"><i class="icon-grid font-16" data-toggle="tooltip" data-placement="left" title="Grid & List Toggle"></i></a></h5>
    <nav id="left-sidebar-nav" class="sidebar-nav">
        <ul class="metismenu">
            <li class="g_heading">Project</li>
            <li class="active"><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>
            <li><a href="{{route('admin.add.product')}}"><i class="fa fa-list-ol"></i><span>Add Products</span></a></li>
            <li><a href="{{route('admin.list.product')}}"><i class="fa fa-calendar-check-o"></i><span>All Products</span></a></li>
            <li><a href="{{route('admin.add.order')}}"><i class="fa fa-check-square-o"></i><span>Add Order</span></a></li>
            <li><a href="{{route('admin.orders')}}"><i class="fa fa-check-square-o"></i><span>Orders</span></a></li>
            <li><a href="{{route('admin.manage.accounts')}}"><i class="fa fa-universal-access"></i><span>Manage Accounts</span></a></li>
            <li><a href="#"><i class="fa fa-photo"></i><span>Gallery</span></a></li>
            <li class="g_heading">Support</li>
            <li><a href="javascript:void(0)"><i class="fa fa-support"></i><span>Need Help?</span></a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-tag"></i><span>ContactUs</span></a></li>
        </ul>
    </nav>
</div>
