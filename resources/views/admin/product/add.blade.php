@extends('layouts.adminLayout')


@push('headerCss')
<style>
    .set-form-group-list-space{
        height: 260px;
        padding-top: 17px;
        border: 2px solid #cccccc;
        border-style: dashed;
        border-radius: 29px;
        text-align: center;
        width: 100%;
        cursor: pointer;
        display: flex;
        justify-content: center;
    }
    .file-input-wrapper > input[type="file"] {
        font-size: 15px;
        position: absolute;
        bottom: 37px;
        opacity: 0;
        height: 36px;
        cursor: pointer;
        width: 173px;
    }
    .file-input-wrapper{
        display: flex;
        justify-content: center;
        position: absolute;
        height: 240px;
        cursor: pointer;
        width: 50%;
    }
    .file-input-wrapper > .btn-file-input {
        display: inline-block;
        height: 40px;
        background-color: #57b029;
        border: 2px solid #57b029;
        color: #fff;
        font-size: 14px;
        font-family: Montserrat, sans-serif !important;
        font-weight: 500;
        border-radius: 0px;
        padding: 1px 25px 2px 25px;
        position: absolute;
        bottom: 35px;
    }

    .graphicImage{
        position: absolute;
        top: 17%;
    }

    .dragdropimages{
        position:relative;
        top:71px;
        color:#555;
        height: 100px;
        font-size: 14px;
        font-family: Montserrat, sans-serif !important;
    }
    .set-remove{
        position: relative;
        top: -80px;
        left: 48px;
    }
    .pip{
        margin-right: 10px;
    }
    .set-bg-color{
        background: #fff;
        padding: 15px;
        margin-top: 12px;
    }
</style>
@endpush

@section('content')
    <div class="container-fluid">
        <div class="set-bg-color">
            <h1>Add Product</h1>
            <br>
            @if(session()->has('success'))
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                </div>
            @endif
            <br>
            <form method="POST" action="{{route('admin.create.product')}}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Name:</label>
                            <input type="text" required name="name" class="form-control" placeholder="Enter Product Name" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Description:</label>
                            <textarea required name="description" class="form-control" placeholder="Enter Product Name" rows="6">
                    </textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group set-form-group-list-space">
                            <img class="graphicImage" src="{{asset('graphicImage.png')}}">
                            <span class="dragdropimages">Upload Images</span>
                            <div class="file-input-wrapper">
                                <button id="addImage" class="btn-file-input">Add More Images
                                </button>
                                <input type="file" class="newFiles" accept="image/*" id="files" multiple name="files[]" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Quantity:</label>
                            <input type="number" required name="qty" class="form-control" placeholder="Enter Product Quantity" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Original Price:</label>
                            <input type="number" required name="original_price" class="form-control" placeholder="Enter Original Price" />
                        </div>
                        <div class="col-md-6">
                            <label>Sale Price:</label>
                            <input type="number" required name="sale_price" class="form-control" placeholder="Enter Sale Price" />
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script>
        var file_num = 0;
        $(document).ready(function() {

            window.file_num = 0;
            $(document).on('click', 'span.imgRemove', function() {
                console.log('please decrement coutner now');
                window.file_num = window.file_num-1;
                if(window.file_num==0)
                {
                    $('.dragdropimages').show();
                    $('.graphicImage').show();
                }
                console.log(window.file_num);
            });

            if (window.File && window.FileList && window.FileReader) {
                $("#files").on("change", function(e) {
                    var files = e.target.files;
                    var filesLength = files.length;
                    console.log('length of files uploaded');
                    console.log(files.length);
                    console.log('length of previous fiels uploaded');
                    console.log(window.file_num);
                    // let totalNumberOfFiles = window.file_num + files.length;
                    console.log('length of all fiels uploaded now');
                    console.log(window.file_num);

                    if(window.file_num+filesLength <= 6 )  {
                        window.file_num = window.file_num+filesLength;
                        var a = [];
                        for (var i = 0; i < filesLength; i++) {
                            var f = files[i]
                            var b = a.push(f)
                            var fileReader = new FileReader();
                            fileReader.onload = (function(e) {
                                $('.graphicImage').hide();
                                $('.dragdropimages').hide();
                                var file = e.target;
                                $("<span class=\"pip\">" +
                                    "<img class=\"imageThumb\" style=\"width:100px;height:70px;object-fit: cover;\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                                    "<br/><span class=\"remove set-remove imgRemove \"><img src=\"/cross-image-list-space.png\"></span>" +
                                    "</span>").insertAfter("#files");
                                $(".remove").click(function(){
                                    var length = $(this).length;
                                    $(this).parent(".pip").remove();
                                    if(length === null) {
                                        $('.graphicImage').show();
                                        $('.dragdropimages').show();
                                    }
                                });
                            });
                            sessionStorage.setItem('length',a);
                            fileReader.readAsDataURL(f);
                        }
                    }
                    else {
                        toastr.error('{{trans('validation.images_limit')}}');
                    }
                });
            } else {
                alert("Your browser doesn't support to File API")
            }
        });
    </script>
@endpush
