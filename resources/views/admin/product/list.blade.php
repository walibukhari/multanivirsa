@extends('layouts.adminLayout')


@push('headerCss')
    <style>
        .custom-add-pencil{
            font-size: 20px;
        }
        .custom-add-trash{
            color: red;
            font-size: 20px;
        }
    </style>
@endpush

@section('content')
    <div class="container-fluid">
        <div class="set-bg-color">
            <br>
            <div class="row">
                <div class="col-md-6">
                    <h1>All Product</h1>
                </div>
                <div class="col-md-6 text-right">
                     <button class="btn btn-outline-info" data-toggle="modal" data-target="#trashModal">Show Trashed Products</button>
                </div>
            </div>
            <br>
            @if(session()->has('success'))
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                </div>
            @endif
            <br>
            <table class="table table-active">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Sale Price</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{$product->product_id}}</td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->description}}</td>
                            <td>{{$product->quantity}}</td>
                            <td>{{$product->sale_price}}</td>
                            <td>
                                <img src="/{{$product->image}}" style="width:50px" />
                            </td>
                            <td>
                                <a href="{{route('admin.edit.product',[$product->id])}}">
                                    <i class="fa fa-pencil-square-o custom-add-pencil"></i>
                                </a>
                                /
                                <a href="javascript:;" onclick="openDeleteModal({{$product->id}})" data-toggle="modal" data-target="#deleteModal">
                                    <i class="fa fa-trash custom-add-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


        <!--delete Modal -->
        <div id="deleteModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <input type="hidden" value="" id="productId" />
                        <p>Are You Sure You Want To Delete !..</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" onclick="deleteProduct()" class="btn btn-success">Confirm</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <!--trash Modal -->
        <div id="trashModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="max-width: 80%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h1>Trahed Products</h1>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Original Price</th>
                                    <th>Sale Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($trashedData as $trashed)
                                    <tr>
                                        <td>{{$trashed->product_id}}</td>
                                        <td>{{$trashed->name}}</td>
                                        <td>{{$trashed->original_price}}</td>
                                        <td>{{$trashed->sale_price}}</td>
                                        <td>
                                            <a href="{{route('admin.restore.product',[$trashed->id])}}" class="btn btn-outline-primary">Revert</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        function openDeleteModal(id){
            $('#productId').val(id);
        }
        function deleteProduct(){
            let id = $('#productId').val();
            let url = '/admin/delete-product/'+id;
            $.ajax({
               url:url,
               method:'GET',

               success: function (response){
                    console.log(response);
                    toastr.success('Product Deleted Successfully');
                    setTimeout(() => {
                       window.location.reload();
                    },1000);
               },
               error: function (error) {
                   console.log(error);
                   toastr.success('Error Something Went Wrong Please try Again');
                   setTimeout(() => {
                       window.location.reload();
                   },1000);
               }
            });
        }
    </script>
@endpush
