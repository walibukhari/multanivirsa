@extends('layouts.adminLayout')


@push('headerCss')
    <style>
        .custom-add-pencil{
            font-size: 20px;
        }
        .custom-add-trash{
            color: red;
            font-size: 20px;
        }
        .select2-container {
            box-sizing: border-box;
            display: inline-block;
            margin: 0;
            position: relative;
            vertical-align: middle;
            width: 100% !important;
        }
    </style>
@endpush

@section('content')
    <div class="container-fluid" id="addOrder">
        <div class="set-bg-color">
            <h1>Add Order</h1>
            <br>
            @if(session()->has('success'))
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                </div>
            @endif
            <br>
            <form class="form-order">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Select Order Id</label>
                            <select class="js-example-basic-single" id="select2Value" name="states[]" multiple="multiple">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success" @click="getOrderData()">
                                <i class="fa fa-refresh fa-spin" v-if="showLoader" aria-hidden="true"></i>
                                <i class="fa fa-refresh" v-if="!showLoader" aria-hidden="true"></i>
                                Search
                            </button>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Qty</th>
                                <th>Original Price</th>
                                <th>Sale Price</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="orders in orderArray">
                                <td>@{{ orders.product_id }}</td>
                                <td>@{{ orders.name }}</td>
                                <td>@{{ orders.description }}</td>
                                <td></td>
                                <td>@{{ orders.original_price }}</td>
                                <td>@{{ orders.sale_price }}</td>
                                <td><img style="width: 100px;" :src="'/'+orders.image" /></td>
                                <td>
                                    <span v-if="orders.status == null">
                                        New Order
                                    </span>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-outline-info" @click="addOrder(orders.id)">
                                        Add Order
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script>
        //start vue js
        new Vue({
            el: '#addOrder',
            data: {
                message: 'Hello Vue!',
                showLoader:false,
                orderArray:[]
            },
            methods:{
                getOrderData: function () {
                    this.showLoader = true;
                    let value = $('#select2Value').val();
                    let url = '/admin/get/orders/data/'+value;
                    this.$http.get(url).then((response) => {
                        if(response.data.status == true) {
                            setTimeout(() => {
                                console.log('response.data.data');
                                console.log(response.data.data);
                                this.orderArray = response.data.data;
                                this.showLoader = false;
                            },1500);
                        }
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    })
                },
                addOrder: function (id) {
                    let url = '{{route('admin.create.orders')}}'
                    let form = {
                        '_token' : '{{csrf_token()}}',
                        'id' : id
                    };
                    this.$http.post(url,form).then((response) => {
                        if(response.data.status == true) {
                            toastr.success('Order Added Successfully');
                        }
                    }).catch((error) => {
                       console.log('error');
                       console.log(error);
                    });
                }
            },
            mounted(){
                console.log(this.message);
                console.log(this.showLoader);
                console.log('start vue js add order page');
            }
        })



        // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
            let url = '{{route('admin.order.numbers')}}';
            $('.js-example-basic-single').select2({
                ajax: {
                    url: url,
                    dataType: 'json',
                    type: "GET",
                    delay: 250,
                    data: function (term) {
                        return {
                            term: term
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        var data_array = []
                        data.map(function (item) {
                            data_array.push({id:item.id,text:item.product_id});
                        });

                        console.log('data_array');
                        console.log(data_array);
                        return {
                            results:data_array
                        }
                    },
                    allowClear: true,
                    minimumInputLength: 1,
                    maximumSelectionLength: 3,
                }
            });
        });
    </script>
@endpush
