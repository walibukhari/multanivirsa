@extends('layouts.adminLayout')


@push('headerCss')
@endpush

@section('content')
    <div class="container-fluid" id="orders">
        <div class="set-bg-color">
            <h1>Orders</h1>
            <br>
            @if(session()->has('success'))
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                </div>
            @endif
            <br>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>Order No</th>
                        <th>Name</th>
                        <th>Total Item</th>
                        <th>Original Price</th>
                        <th>Sale Price</th>
                        <th>Status</th>
                        <th>Profit</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($orders) > 0)
                    @foreach($orders as $order)
                        <tr>
                            <td>{{$order['order_no']}}</td>
                            <td>{{$order['name']}}</td>
                            <td>
                                @php
                                    $total_item = $order->getQuantity($order['product_id']);
                                @endphp
                                {{$total_item}}
                            </td>
                            <td>{{$order['original_price']*$total_item}}</td>
                            <td>{{$order['sale_price']*$total_item}}</td>
                            <td>
                                @if($order['status'] == \App\Models\Product::STATUS_ADD_ORDER)
                                    <span style="color: blue;">In Progress</span>
                                @endif
                            </td>
                            <td>
                                @php
                                    $profit = $order['sale_price'] - $order['original_price'];
                                    $total_profit = $profit * $total_item;
                                @endphp
                                {{$total_profit}}
                            </td>
                            <td>
                                <button class="btn btn-sm btn-outline-primary" @click="placeOrder('{{$order['id']}}','{{$order['product_id']}}','{{$order['status']}}')">Place Order</button>
                                <a href="{{route('admin.check.order.detail',[$order['product_id'],$order['status']])}}" class="btn btn-sm btn-outline-info">Check Detail</a>
                                <button class="btn btn-sm btn-outline-danger" @click="deleteOrder('{{$order['id']}}','{{$order['product_id']}}','{{$order['status']}}')">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="set-bg-color">
            <h1>Orders Placed</h1>
            <br>
            @if(session()->has('success'))
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                </div>
            @endif
            <br>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>Order No</th>
                        <th>Name</th>
                        <th>Total Item</th>
                        <th>Original Price</th>
                        <th>Sale Price</th>
                        <th>Status</th>
                        <th>Profit</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($placed_orders) > 0)
                        @foreach($placed_orders as $order)
                        <tr>
                            <td>{{$order['order_no']}}</td>
                            <td>{{$order['name']}}</td>
                            <td>
                                @php
                                    $total_item = $order->getQuantityPlaceOrder($order['product_id']);
                                @endphp
                                {{$total_item}}
                            </td>
                            <td>{{$order['original_price']*$total_item}}</td>
                            <td>{{$order['sale_price']*$total_item}}</td>
                            <td>
                                @if($order['status'] == \App\Models\Order::STATUS_ORDER_PLACED)
                                    <span style="color: blue;">Order Placed</span>
                                @endif
                            </td>
                            <td>
                                @php
                                    $profit = $order['sale_price'] - $order['original_price'];
                                    $total_profit = $profit * $total_item;
                                @endphp
                                {{$total_profit}}
                            </td>
                            <td>
                                <button class="btn btn-outline-primary" @click="shipped('{{$order['id']}}','{{$order['product_id']}}','{{$order['status']}}')">Shipped</button>
                                <a href="{{route('admin.check.order.detail',[$order['product_id'],$order['status']])}}" class="btn btn-sm btn-outline-info">Check Detail</a>
                                <button class="btn btn-sm btn-outline-danger" @click="deleteOrder('{{$order['id']}}','{{$order['product_id']}}','{{$order['status']}}')">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="set-bg-color">
            <h1>Orders Shipped</h1>
            <br>
            @if(session()->has('success'))
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                </div>
            @endif
            <br>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>Order No</th>
                        <th>Name</th>
                        <th>Total Item</th>
                        <th>Original Price</th>
                        <th>Sale Price</th>
                        <th>Status</th>
                        <th>Profit</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($shipped_orders as $order)
                        <tr>
                            <td>{{$order['order_no']}}</td>
                            <td>{{$order['name']}}</td>
                            <td>
                                @php
                                    $total_item = $order->getQuantityShippedOrder($order['product_id']);
                                @endphp
                                {{$total_item}}
                            </td>
                            <td>{{$order['original_price']*$total_item}}</td>
                            <td>{{$order['sale_price']*$total_item}}</td>
                            <td>
                                @if($order['status'] == \App\Models\Order::STATUS_ORDER_SHIPPED)
                                    <span style="color: blue;">Shipped</span>
                                @endif
                            </td>
                            <td>
                                @php
                                    $profit = $order['sale_price'] - $order['original_price'];
                                    $total_profit = $profit * $total_item;
                                @endphp
                                {{$total_profit}}
                            </td>
                            <td>
                                <button class="btn btn-outline-primary" @click="paymentReceived('{{$order['id']}}','{{$order['product_id']}}','{{$order['status']}}')">Payment Received</button>
                                <a href="{{route('admin.check.order.detail',[$order['product_id'],$order['status']])}}" class="btn btn-sm btn-outline-info">Check Detail</a>
                                <button class="btn btn-sm btn-outline-danger" @click="deleteOrder('{{$order['id']}}','{{$order['product_id']}}','{{$order['status']}}')">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="set-bg-color">
            <h1>Orders Failed</h1>
            <br>
            @if(session()->has('success'))
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                </div>
            @endif
            <br>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>Order No</th>
                        <th>Name</th>
                        <th>Original Price</th>
                        <th>Sale Price</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Profit</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders_failed as $order)
                        <tr>
                            <td>{{$order['order_no']}}</td>
                            <td>{{$order['name']}}</td>
                            <td>{{$order['original_price']}}</td>
                            <td>{{$order['sale_price']}}</td>
                            <td>{{$order->getQuantity($order['product_id'])}}</td>
                            <td>
                                @if($order['status'] == \App\Models\Order::STATUS_ORDER_DELIVER_FAILED)
                                    <span style="color: red;">Failed</span>
                                @endif
                            </td>
                            <td>
                                @php
                                    $profit = $order['sale_price'] - $order['original_price'];
                                @endphp
                                {{$profit}}
                            </td>
                            <td>
                                <button class="btn btn-outline-primary">Place Order</button>
                                <button class="btn btn-outline-info">Check Order Detail</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="set-bg-color">
            <h1>Orders Completed</h1>
            <br>
            @if(session()->has('success'))
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                </div>
            @endif
            <br>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>Order No</th>
                        <th>Name</th>
                        <th>Original Price</th>
                        <th>Sale Price</th>
                        <th>Status</th>
                        <th>Profit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($orders_complete) > 0)
                    @foreach($orders_complete as $order)
                        @if(isset($order->order))
                        <tr>
                            <td>{{$order->order->order_no}}</td>
                            <td>{{$order->order->name}}</td>
                            <td>{{$order->order->original_price}}</td>
                            <td>{{$order->order->sale_price}}</td>
                            <td>
                                @if($order->status == \App\Models\Order::STATUS_ORDER_DELIVER_PAYMENT_RECEIVED_SUCCESS)
                                    <span style="color: green;">Completed</span>
                                @endif
                            </td>
                            <td>
                                @php
                                    $profit = $order->order->sale_price - $order->order->original_price;
                                @endphp
                                {{$profit}}
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        new Vue({
            el: '#orders',
            data: {
                message: 'Hello Vue!',
                showLoader:false,
                orderArray:[]
            },
            methods:{
                paymentReceived: function (order_id,product_id,status){
                    console.log(order_id,product_id,status);
                    let url ='/admin/payment-received/'+order_id+'/'+product_id+'/'+status;
                    this.$http.get(url).then((response) => {
                        console.log('response');
                        console.log(response);
                        if(response.data.status == true) {
                            toastr.success(response.data.message);
                        }
                        setTimeout(() => {
                            window.location.reload();
                        },1500);
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    });

                },

                shipped: function (order_id,product_id,status){
                    let url ='/admin/shipped-orders/'+order_id+'/'+product_id+'/'+status;
                    this.$http.get(url).then((response) => {
                        console.log('response');
                        console.log(response);
                        if(response.data.status == true) {
                            toastr.success(response.data.message);
                        }
                        setTimeout(() => {
                            window.location.reload();
                        },1500);
                    }).catch((error) => {
                       console.log('error');
                       console.log(error);
                    });
                },
                deleteOrder: function (order_id,product_id,status){
                    let url ='/admin/delete-order/'+order_id+'/'+product_id+'/'+status;
                    this.$http.get(url).then((response) => {
                        console.log('response');
                        console.log(response);
                        toastr.success(response.data.message);
                        setTimeout(() => {
                            window.location.reload();
                        },1500);
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    })
                },
                placeOrder: function (order_id,product_id,status){
                    console.log(order_id,product_id);
                    let url ='/admin/place-order/'+order_id+'/'+product_id+'/'+status;
                    this.$http.get(url).then((response) => {
                        if(response.data.status == true) {
                            toastr.success(response.data.message);
                            setTimeout(() => {
                                window.location.reload();
                            },1500);
                        }
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    })
                }
            },
            mounted(){
                console.log('start vue js orders page');
            }
        })
    </script>
@endpush
