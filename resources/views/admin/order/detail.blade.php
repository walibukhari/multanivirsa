@extends('layouts.adminLayout')


@push('headerCss')
@endpush

@section('content')
    <div class="container-fluid" id="orders">
        <div class="set-bg-color">
            <h1>Order Detail</h1>
            <br>
            @if(session()->has('success'))
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                </div>
            @endif
            <br>
            @if(isset($orders[0]))
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Name</label>
                        <input type="text" value="{{$orders[0]->name}}" readonly class="form-control" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Quantity</label>
                        <input type="text" value="{{$orders[0]->getQuantity($orders[0]->product_id)}}" readonly class="form-control" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Price</label>
                        <input type="text" value="{{$orders[0]->original_price}}" readonly class="form-control" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Description</label>
                        <textarea readonly class="form-control" rows="12">{{$orders[0]->product->description}}</textarea>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Profit</label>
                        @php
                            $profit = $orders[0]->sale_price - $orders[0]->original_price;
                        @endphp
                        <input type="text" value="{{$profit}}" readonly class="form-control" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>Images</label>
                        <div class="box-images">
                            @foreach($orders[0]->product->product_images as $images)
                                <img src="/{{$images->image}}" style="width: 100px;height: 100px;margin-right: 10px;cursor: pointer;" />
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
@endsection

@push('js')
@endpush
