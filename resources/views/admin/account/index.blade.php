@extends('layouts.adminLayout')


@push('headerCss')
@endpush

@section('content')
    <div class="container-fluid" id="accounts">
        <div class="set-bg-color">
            <h1>Manage Accounts</h1>
            <br>
            @if(session()->has('success'))
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                </div>
            @endif
            <br>
            <hr>
            <h1>Add Amount</h1>
            <form class="form">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Name:</label>
                            <input type="text" name="name" v-model="name" placeholder="enter amount holder name" class="form-control" />
                        </div>
                        <div class="col-md-6">
                            <label>Amount:</label>
                            <input type="number" name="amount" v-model="amount" placeholder="enter amount" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-outline-info" @click="saveAccounts()">Save</button>
                        </div>
                    </div>
                </div>
            </form>
            <br>
            <hr>
            <h1>Total Amount</h1>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($amounts as $amount)
                        <tr>
                            <td>{{$amount->name}}</td>
                            <td>{{$amount->amount}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td><b>Total Balnce</b></td>
                        <td><b>{{$total_amount}}</b></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <hr>
            <br>
            <hr>
            <h1>Total Sale Orders</h1>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Original Price</th>
                            <th>Sale Price</th>
                            <th>Status</th>
                            <th>Profit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders_complete as $order)
                            <tr>
                                <td>{{$order->order->name}}</td>
                                <td>{{$order->order->original_price}}</td>
                                <td>{{$order->order->sale_price}}</td>
                                <td style="color: green;">{{$order->getStatus($order->order->status)}}</td>
                                <td>
                                    @php
                                        $profit = $order->order->sale_price - $order->order->original_price;
                                    @endphp
                                    {{$profit}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="col-md-12">
                    {{$orders_complete->links()}}
                </div>
            </div>
            <hr>
            <br>
            <h1>Accounts Summary</h1>
            <hr>
            <b>Total Balance:</b> <b>{{$total_amount - $total_original_price}}</b>&nbsp;&nbsp;&nbsp;&nbsp;<small>(right now our total balance)</small><br><br>
            <b>Total Orders:</b> <b>{{$total_orders}}</b>&nbsp;&nbsp;&nbsp;&nbsp;<small>(right now our total orders)</small><br><br>
            <b>Total Original Price:</b> <b>{{$total_original_price}}</b><br><br>
            <b>Total Sale Price:</b> <b>{{$total_sale_price}}</b><br><br>
            <b>Total Profit Amount:</b> <b>{{$total_profit}}</b>&nbsp;&nbsp;&nbsp;&nbsp;<small>(right now our total profit)</small><br><br>
            <br><br>
            <b>Our Grand Total Balance:</b> <b>{{$total_sale_price + $total_profit}}</b>&nbsp;&nbsp;&nbsp;&nbsp;<small>(right now our grand total balance)</small><br><br>
        </div>
    </div>
@endsection

@push('js')
    <script>
        new Vue({
            el: '#accounts',
            data: {
                name:'',
                amount:'',
                message: 'Hello Vue!',
                showLoader:false,
                orderArray:[]
            },
            methods:{
                saveAccounts:function (){
                    console.log(this.name);
                    console.log(this.amount);
                    let form = {
                      '_token':'{{csrf_token()}}',
                      'name':this.name,
                      'amount' : this.amount
                    };
                    let url = '{{route('admin.add.accounts')}}';
                    this.$http.post(url,form).then((response) => {
                       console.log('response');
                       console.log(response);
                       if(response.data.status == true) {
                           toastr.success(response.data.message);
                       }
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    });
                }
            },
            mounted(){
                console.log('start vue js admin accounts');
            }
        })
    </script>
@endpush
