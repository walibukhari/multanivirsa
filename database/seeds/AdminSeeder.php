<?php

use App\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
           'email' => 'admin@multanivirsa.com',
           'password' => bcrypt('secret123'),
           'status' => 1
        ]);
    }
}
